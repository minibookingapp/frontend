import Vue from 'vue';
import Vuetify from 'vuetify';
import 'vuetify/dist/vuetify.min.css';
import '@fortawesome/fontawesome-free/css/all.css'

Vue.use(Vuetify);

// @ts-ignore
export default new Vuetify({
    theme: {
        themes: {
            light: {
                primary: "#bb3819"
            }
        }
    },
    icons: {
        iconfont: 'fa',
    },
});