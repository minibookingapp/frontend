import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';

Vue.use(Router);

export default new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home,
        },
        {
            path: '/units',
            name: 'units',
            component: () => import('./views/Units.vue'),
            children: [
                {
                    path: "new",
                    name: "unit-new",
                    component: () => import("./views/UnitNew.vue")
                },
                {
                    path: "edit/:id",
                    name: 'unit-edit',
                    component: () => import('./views/UnitEdit.vue')
                }
            ]
        },
        {
            path: '/reservations',
            name: 'reservations',
            component: () => import('./views/Reservations.vue'),
            children: [
                {
                    path: "edit/:id",
                    name: 'reservations-edit',
                    component: () => import('./views/ReservationEdit.vue')
                }
            ]
        },
        {
            path: '/new-reservation',
            name: 'new-reservation',
            component: () => import('./views/ReservationNew.vue'),
        }
    ],
})
;
