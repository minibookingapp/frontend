import axios from 'axios';

interface IUnit {
    name: string,
    price_per_night: number,
    max_persons: number
}

interface IReservation {
    unit_id: string,
    date_from: string,
    date_to: string,
    first_name: string,
    last_name: string,
    email: string,
    total_price: string
}

export default class AxiosModel {
    base = () => process.env.VUE_APP_SERVER_URL;
    headers = () => ({
        'Content-Type': 'application/json'
    });
    getUnits = () => {
        return axios.get(this.base() + 'units', {
            headers: this.headers()
        })
    }
    getUnitDetails = (id: string) => {
        return axios.get(this.base() + 'units/' + id, {
            headers: this.headers()
        })
    }

    editUnit = (id: string, params: IUnit) => {
        return axios.put(this.base() + 'units/' + id, params, {
            headers: this.headers()
        })
    }

    deleteUnit = (id: string) => {
        return axios.delete(this.base() + 'units/' + id, {
            headers: this.headers()
        })
    }

    addUnit = (params: IUnit) => {
        return axios.post(this.base() + 'units', params, {
            headers: this.headers()
        })
    }

    searchUnits = (search?: string) => {
        const searchValue = search ? '/' + search : '';
        return axios.get(this.base() + "searchUnits" + searchValue, {
            headers: this.headers()
        })
    }

    getBlockedDates = (id?: string) => {
        const reservationId = id ? '/' + id : '';
        return axios.get(this.base() + "getBlockedDates" + reservationId, {
            headers: this.headers()
        })
    }

    getReservations = () => {
        return axios.get(this.base() + "reservations", {
            headers: this.headers()
        })
    }

    getReservationDetails = (id: string) => {
        return axios.get(this.base() + 'reservations/' + id, {
            headers: this.headers()
        })
    }

    addReservation = (params: IReservation) => {
        return axios.post(this.base() + 'reservations', params, {
            headers: this.headers()
        })
    }

    editReservation = (id: string, params: IReservation) => {
        return axios.put(this.base() + 'reservations/' + id, params, {
            headers: this.headers()
        })
    }

    deleteReservation = (id: string) => {
        return axios.delete(this.base() + 'reservations/' + id, {
            headers: this.headers()
        })
    }
}